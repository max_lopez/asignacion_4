import redis 

client = redis.Redis(host = '127.0.0.1', port = 6379)

#Ingresar datos en la base de datos 
client.Hset("Usuario","Nombre","Max")
client.Hset("Usuario","Apellido","Lopez")
client.Hset("Usuario","Edad","18")
client.Hset("Usuario","correo","maxlopzb1701@gmail.com")

#Ver datos introducidos
print(client.hgetall("Usuario"))

#Borrar datos introducidos 
client.HDEL("Usuario","Nombre")

#Borrar todos los datos 
client.HDELALL("Usuario")

#Modificar un dato 
client.Hset("Usuario","Nombre","Juanito")

